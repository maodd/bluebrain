//
//  BlueBrainAppDelegate.h
//  BlueBrain
//
//  Created by Bijay Ghosh on 9/24/13.
//  Copyright (c) 2013 Capital Numbers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "ViewController.h"
#import <MessageUI/MFMailComposeViewController.h>


@interface BlueBrainAppDelegate : UIResponder <UIApplicationDelegate, MFMailComposeViewControllerDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong,nonatomic) ViewController *objViewController;
@property (strong,nonatomic) UINavigationController *nav;
@property (strong, nonatomic) NSDictionary * appCfg;

@property (nonatomic, strong) MFMailComposeViewController* mailComposeViewController;

+ (BlueBrainAppDelegate*)sharedInstance;


- (NSString*)appName;

@end
