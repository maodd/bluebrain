//
//  ViewController.m
//  BlueBrain
//
//  Copyright (c) 2014 Capitalnumbers. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController
@synthesize objTutorialViewController;
@synthesize SplashView;

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    NSURL *url;
     url = [[NSBundle mainBundle] URLForResource:@"Intro" withExtension:@"gif"];
//    if ([[UIScreen mainScreen] bounds].size.height == 568.0) {
//        url = [[NSBundle mainBundle] URLForResource:@"Intro-640x1136" withExtension:@"gif"];
//
//    } else {
//        url = [[NSBundle mainBundle] URLForResource:@"Intro-640x960" withExtension:@"gif"];
//        
//    }
    

    
 
    
   
    
//    [SplashView loadRequest: [NSURLRequest requestWithURL:url]];

}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    
    sleep(3);
    
    if  ([[NSUserDefaults standardUserDefaults] boolForKey:@"tutorial_played"]){
        MainScreen *objMainScreen  = [self getMainScreen];
        [self.navigationController pushViewController:objMainScreen animated:NO];
    }else{
        [self performSelector:@selector(goToTutorialScreen) withObject:nil afterDelay:0];
        //self.mainScreen = objMainScreen;
        //[objMainScreen view];
    }
    
}
- (MainScreen *)getMainScreen
{
    MainScreen *objMainScreen;

    {
        objMainScreen = [[MainScreen alloc] initWithNibName:@"MainScreen_iPhone" bundle:nil];
    }
   
    
    return objMainScreen;
  

}
-(void)goToTutorialScreen
{
    [[NSURLCache sharedURLCache] removeAllCachedResponses];
    //[self.SplashView loadHTMLString:@"<html></html>" baseURL:nil];


    {
        objTutorialViewController = [[TutorialViewController alloc] initWithNibName:@"TutorialViewController_iPhone" bundle:nil];
    }
   
    
    [self.navigationController pushViewController:objTutorialViewController animated:NO];

    
//    [self.navigationController presentViewController:objTutorialViewController animated:true completion:^{
//        
//    }];
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
