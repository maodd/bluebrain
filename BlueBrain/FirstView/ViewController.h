//
//  ViewController.h
//  BlueBrain
//
//  Copyright (c) 2014 Capitalnumbers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorialViewController.h"

@interface ViewController : UIViewController
@property (strong,nonatomic) TutorialViewController *objTutorialViewController;
@property (strong, nonatomic) IBOutlet UIWebView *SplashView;
@property (strong, nonatomic) MainScreen *mainScreen;
@end
