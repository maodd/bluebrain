//
//  Zone.h
//  BlueBrain
//
//  Created by Bijay Ghosh on 11/5/13.
//  Copyright (c) 2013 Capital Numbers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Zone : NSObject

@property (retain,nonatomic) NSString *Name;
@property (retain,nonatomic) NSMutableArray *ArrHotspot;
@property (retain,nonatomic) NSMutableArray *ArrZoneCorner;

@end
