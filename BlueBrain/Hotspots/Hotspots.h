//
//  Hotspots.h
//  BlueBrain
//
//  Created by Bijay Ghosh on 11/5/13.
//  Copyright (c) 2013 Capital Numbers. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import "TheAmazingAudioEngine.h"

@interface Hotspots : NSObject
@property (retain,nonatomic) NSString *Name,*Filename,*Looping,*Radius,*Latitude,*Longitude,*FadeInRate,*FadeValue,*CurrentVolume;

@property int Hotspotdistance,LastHotspotdistance;

@property (nonatomic, retain) CLLocation *Hotspot;

@property (nonatomic, retain) AEAudioFilePlayer *track;

@end
