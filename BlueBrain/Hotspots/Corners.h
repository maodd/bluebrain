//
//  Corners.h
//  BlueBrain
//
//  Created by Bijay Ghosh on 12/10/13.
//  Copyright (c) 2013 Capital Numbers. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Corners : NSObject

@property (retain,nonatomic) NSString *Latitude,*Longitude;
@end
