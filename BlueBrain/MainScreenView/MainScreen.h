//
//  MainScreen.h
//  BlueBrain
//
//  Copyright (c) 2014 Capitalnumbers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "InformationView.h"
#import <CoreLocation/CoreLocation.h>
#import "Composition.h"
#import "Corners.h"
#import "Zone.h"
#import "Hotspots.h"
#import "TheAmazingAudioEngine.h"
#import <MessageUI/MFMailComposeViewController.h>
#import <MapKit/MapKit.h>

@interface MainScreen : UIViewController<UIScrollViewDelegate,CLLocationManagerDelegate,NSXMLParserDelegate,
MFMailComposeViewControllerDelegate,
MKMapViewDelegate>
{
 
    AEChannelGroupRef _group;
    AudioFileID _audioUnitFile;
}
@property (nonatomic, retain) NSTimer *myTimer;
@property (nonatomic, retain) AEAudioController *audioController;
@property (nonatomic, retain) AEAudioUnitChannel *audioUnitPlayer;
@property (nonatomic, retain) CLLocation *BestLocation;
@property (nonatomic, retain) NSMutableString *curElem;
@property (nonatomic, retain) NSString *parentTag;
@property (nonatomic, retain) NSMutableArray *ArrHotspot;
@property (copy, readonly) Composition *BBComposition;
@property (copy, readonly) Corners *BBCorners;
@property (copy, readonly) Zone *BBZone;
@property (copy, readonly) Hotspots *CurrentHotspot;


@property (retain, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (retain, nonatomic) IBOutlet UIView *secondMainView;
@property (retain, nonatomic) IBOutlet UIView *topView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@property (retain, nonatomic) IBOutlet UIView *bottomView;
@property (retain, nonatomic) IBOutlet UILabel *siteNameLabel;
- (IBAction)informationBtnClicked:(id)sender;

@end
