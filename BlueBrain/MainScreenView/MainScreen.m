//
//  MainScreen.m
//  BlueBrain
//
//  Copyright (c) 2014 Capitalnumbers. All rights reserved.
//

#import "MainScreen.h"


@interface MainScreen ()

@property (nonatomic, strong) MFMailComposeViewController* mailComposeViewController;
@property (nonatomic, retain) MKMapView *mapView;
@property (strong, nonatomic)  CLLocationManager *locationManager;
@end

@implementation MainScreen



@synthesize audioController,BestLocation,locationManager;

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@synthesize CurrentHotspot,curElem,parentTag,ArrHotspot,BBComposition,BBCorners,BBZone;


int ThreadCount;
int isFirstPing;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.automaticallyAdjustsScrollViewInsets = NO;
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    _scrollView.contentSize = CGSizeMake(320, [[BlueBrainAppDelegate sharedInstance].appCfg[@"ScrollViewContentHeight"] floatValue]);
    
    _topView.backgroundColor = [UIColor colorWithRed:28.0/255.0 green:21.0/255.0 blue:40.0/255.0 alpha:1.0];
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0)
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            _secondMainView.frame = CGRectMake(0, 0, 320, 480);
        }
        else{
            _secondMainView.frame = CGRectMake(0, 0, 768, 1024);
        }
    }
//    _scrollView.frame = CGRectMake(0, 0, self.bottomView.frame.size.width, self.bottomView.frame.size.height);
//   
//    [self.bottomView addSubview:_scrollView];

    locationManager = [[CLLocationManager alloc] init];
    
    NSString *xmlPath = [[NSBundle mainBundle] pathForResource:kXmlFileName ofType:@"xml"];
    NSData *xmlData = [NSData dataWithContentsOfFile:xmlPath];
    NSXMLParser *xmlParser = [[NSXMLParser alloc] initWithData:xmlData];
    [xmlParser setDelegate:self];
    [xmlParser parse];
    
    [self getCurrentZone];
    
    
    [self UpdateCurrentLocation:nil];
    
    
    //stupid!!!
//    self.myTimer = [NSTimer scheduledTimerWithTimeInterval:0.5 target:self selector:@selector(UpdateCurrentLocation:) userInfo:nil repeats:YES];
    // Do any additional setup after loading the view from its nib.
}
- (IBAction)informationBtnClicked:(id)sender {
    InformationView *objInformationView = [[InformationView alloc] initWithNibName:@"InformationView_iPhone" bundle:nil];
    [self.navigationController pushViewController:objInformationView animated:YES];
}

-(void) setupAudioFile
{
    ThreadCount=3;
    isFirstPing=0;
    int index=0;
    
    self.audioController=nil;
    self.audioController = [[[AEAudioController alloc] initWithAudioDescription:[AEAudioController nonInterleaved16BitStereoAudioDescription]  inputEnabled:NO] autorelease];
    self.audioController.preferredBufferDuration = 0.005;
    self.audioController.allowMixingWithOtherApps = NO;
    
    [self.audioController start:NULL];
    
    for (int i=0; i<[self.ArrHotspot count]; i++) {
        Hotspots *obj= [self.ArrHotspot objectAtIndex:i];
        
        obj.Hotspot=[[CLLocation alloc] initWithLatitude:[obj.Latitude doubleValue] longitude:[obj.Longitude doubleValue]];
        obj.track=nil;
        
        // check if same audio file being used by different zone. if yes, just set one acive track
        for (int r=0; r<i; r++)
        {
            Hotspots *objr= [self.ArrHotspot objectAtIndex:r];
            if ([obj.Filename isEqualToString:objr.Filename])
            {
                index++;
                obj.track=objr.track;
                break;
            }
        }
        if (index<1) {
            NSString * file = [obj.Filename substringToIndex:[obj.Filename length] - 4];
            NSLog(@"file: %@", file);
            NSURL * URL = [[NSBundle mainBundle] URLForResource:file withExtension:@"m4a"];
            
            AEAudioFilePlayer *track = [AEAudioFilePlayer audioFilePlayerWithURL:URL                                                                            error:NULL];
          
            track.volume = 0.0;
            track.channelIsMuted = NO;
            track.loop = YES;
            obj.track=track;
        }
        else
        {
            index=0;
        }
    }
    
    // Create an audio unit channel (a file player)
    self.audioUnitPlayer = [[[AEAudioUnitChannel alloc] initWithComponentDescription:AEAudioComponentDescriptionMake(kAudioUnitManufacturer_Apple, kAudioUnitType_Generator, kAudioUnitSubType_AudioFilePlayer)
                                                                                                                                            ] autorelease];
    
    // Create a group for loop1, loop2 and oscillator
    _group = [self.audioController createChannelGroup];
    
    NSMutableArray *channelsToAdd=[[NSMutableArray alloc] init];
    

    for (int i=0; i<[self.ArrHotspot count]; i++)
    {
        Hotspots *obj= [self.ArrHotspot objectAtIndex:i];
        
        for (int r=0; r<i; r++)
        {
            Hotspots *objr= [self.ArrHotspot objectAtIndex:r];
            if ([obj.Filename isEqualToString:objr.Filename])
            {
                index++;
            }
        }
        if (index<1) {
            if (obj.track == nil){
                NSLog(@"obj.track is nil for file: %@", obj.Filename);
            }else{
                [channelsToAdd addObject:obj.track];
            }
        }
        else
        {
            index=0;
        }
        
        
    }
    [self.audioController addChannels:channelsToAdd toChannelGroup:_group];
    
    // Finally, add the audio unit player
    [self.audioController addChannels:[NSArray arrayWithObjects:_audioUnitPlayer, nil]];
    
    for (int i=0; i<[self.ArrHotspot count]; i++)
    {
        Hotspots *obj= [self.ArrHotspot objectAtIndex:i];
        obj.Hotspotdistance=-1;
    }
    for (int i=0; i<[self.ArrHotspot count]; i++)
    {
        Hotspots *obj= [self.ArrHotspot objectAtIndex:i];
        obj.LastHotspotdistance=0;
    }
    
}
-(void)removeAudioFile
{
    if ( _audioUnitFile ) {
        AudioFileClose(_audioUnitFile);
    }
    
    NSMutableArray *channelsToRemove=[[NSMutableArray alloc] init];
    int index=0;
    for (int i=0; i<[self.ArrHotspot count]; i++)
    {
        Hotspots *obj= [self.ArrHotspot objectAtIndex:i];
        
        for (int r=0; r<i; r++)
        {
            Hotspots *objr= [self.ArrHotspot objectAtIndex:r];
            if ([obj.Filename isEqualToString:objr.Filename])
            {
                index++;
            }
        }
        if (index<1) {
            [channelsToRemove addObject:obj.track];
            obj.track=nil;        }
        else
        {
            index=0;
        }
        
        
    }
    
    [self.audioController removeChannels:channelsToRemove];
    self.audioController=nil;
}

- (void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status{
    NSLog(@"status: %d", status);
    if (status == kCLAuthorizationStatusAuthorizedAlways
        || status == kCLAuthorizationStatusAuthorizedWhenInUse) {
       [locationManager startUpdatingLocation];
    }
    
}
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
    NSLog(@"location error: %@", [error localizedDescription]);
}
- (void)locationManager:(CLLocationManager *)manager
     didUpdateLocations:(NSArray *)locations
{
     [self handleLocationFix:locations.lastObject];
}
- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
{
//    [locationManager stopUpdatingLocation];
    
    
    [self handleLocationFix:newLocation];
}

- (void)handleLocationFix:(CLLocation*)newLocation
{
    NSLog(@"new location: %@", newLocation);
    if (BestLocation==NULL)
    {
        BestLocation=[[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
    }
    
//    int distance = [newLocation distanceFromLocation:BestLocation];
    
//    if (distance<100000)
    {

        
        BestLocation=[[CLLocation alloc] initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude];
        [self setHotspotsDistance:BestLocation];

//        [self setHotspotsVolume];
        
        [self displayHotspotDistance];
 
    }

}

- (void)requestLocationPermission{
    
    if(IS_OS_8_OR_LATER)
    {
        SEL requestSelector = NSSelectorFromString(@"requestAlwaysAuthorization");
        if ([CLLocationManager authorizationStatus] == kCLAuthorizationStatusNotDetermined &&
            [locationManager respondsToSelector:requestSelector]) {
            [locationManager performSelector:requestSelector withObject:NULL];
        } else {
//            [locationManager startUpdatingLocation];
        }
    }
    else
    {
//        [locationManager startUpdatingLocation];
    }
}

-(void)UpdateCurrentLocation:(NSTimer *)timer
{
    
    locationManager.delegate = self;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    locationManager.distanceFilter=5;
    if(IS_OS_8_OR_LATER)
    {
        SEL requestSelector = NSSelectorFromString(@"requestAlwaysAuthorization");
        if ([CLLocationManager authorizationStatus]
            != kCLAuthorizationStatusAuthorizedWhenInUse
            && [CLLocationManager authorizationStatus]
            != kCLAuthorizationStatusAuthorizedAlways
            &&
            [locationManager respondsToSelector:requestSelector]) {
            
            [locationManager performSelector:requestSelector withObject:NULL];

//            UIAlertController * ac = [UIAlertController alertControllerWithTitle:@"Permission Request"
//                                                                         message:@"This musical experience requires that you provide permission to always access your location. This can be changed at anytime via Settings." preferredStyle:UIAlertControllerStyleAlert];
//            [ac addAction:[UIAlertAction actionWithTitle:@"Yes, give location permission"
//               style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
//
//
//
//                       [locationManager performSelector:requestSelector withObject:NULL];
//
//                   }]];
//
//            [ac addAction:[UIAlertAction actionWithTitle:@"No, exit app now"
//                                                  style:UIAlertActionStyleCancel
//                                                handler:^(UIAlertAction * _Nonnull action) {
//
//                                                    UIApplication *app = [UIApplication sharedApplication];
//                                                    [app performSelector:@selector(suspend)];
//
//                                                    //wait 2 seconds while app is going background
//                                                    [NSThread sleepForTimeInterval:2.0];
//
//                                                    //exit app when app is in background
//                                                    exit(0);
//
//                                                }]];
//
//           [self presentViewController:ac animated:YES completion:nil];
            
        } else {
            [locationManager startUpdatingLocation];
        }
    }
    else
    {
        [locationManager startUpdatingLocation];
    }

    
    ///? what is this for?
    if (ThreadCount<3)
    {
        ThreadCount++;
        [self setHotspotsVolume];
        [self getCurrentZone];
    }
    else
    {
        ThreadCount=0;
    }
    
}



-(void)viewWillDisappear:(BOOL)animated
{

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)setVolume:(float)volume onTrack:(AEAudioFilePlayer*)track
{
 
   // NSLog(@"start setting track new volume to %f", volume);
//    dispatch_async(dispatch_get_main_queue(), ^{
        while (fabs(track.volume - volume) > 0.01f) {
            sleep(0.99);
            float diff = (volume - track.volume) * 0.001;
            float newVolume = track.volume;
            newVolume += diff;
            //NSLog(@"setting track new volume to %f", newVolume);
            
            
            /*
            if (newVolume < 0) {
                track.volume = 0;
            //    track.channelIsMuted = YES;
                break;
            }
             */
            track.volume = newVolume;
            track.channelIsPlaying = YES;

            
        }

//    track.volume = volume;
    
 //   });
    
}

-(void)setHotspotsDistance:(CLLocation*)CurrentLocation
{
    
    for (int i=0; i<[self.ArrHotspot count]; i++) {
        Hotspots *obj= [self.ArrHotspot objectAtIndex:i];
        if (obj.LastHotspotdistance==-1) {
            obj.LastHotspotdistance=obj.Hotspotdistance;
        }
        obj.Hotspotdistance = [CurrentLocation distanceFromLocation:obj.Hotspot];
        
    }
    
//    if (isFirstPing==0)
    {
        
        isFirstPing=1;
        int index=0;
        for (int i=0; i<[self.ArrHotspot count]; i++) {
            Hotspots *obj= [self.ArrHotspot objectAtIndex:i];
            
            for (int r=0; r<i; r++)
            {
                Hotspots *objr= [self.ArrHotspot objectAtIndex:r];
                if ([obj.Filename isEqualToString:objr.Filename])
                {
                    if (objr.Hotspotdistance<=[objr.Radius intValue])
                    {
                        index++; //???
                    }
                    
                }
            }
            if (index<1)
            {
                if (obj.Hotspotdistance<=[obj.Radius intValue])
                {
                    [self setVolume:(0.8* [obj.Radius intValue]-obj.Hotspotdistance)/[obj.Radius intValue]  onTrack:obj.track];
                    NSLog(@"playing %@  at volume %f",  obj.Filename, obj.track.volume);


                    
 
                    if (obj.track.volume > 0.1) {
                        self.siteNameLabel.text = obj.Name;
                    }
                } else
                {
                    if (obj.track.volume != 0)
                    {
                         NSLog(@"muting %@  at volume %f",  obj.Filename, obj.track.volume);
                        [self setVolume:0.0 onTrack:obj.track];
                        obj.track.channelIsPlaying = NO;
                        //obj.track.channelIsMuted = YES;
                    }
                    
                    self.siteNameLabel.text = @"";
                }
                
                [self debugHotspot:obj index:i];
                
                
            }
           else
            {
                index=0;
            }
           
            obj.LastHotspotdistance=obj.Hotspotdistance;
        }
    }
    
}

- (void)displayHotspotDistance
{
    if (isFirstPing==1) {
        
        NSMutableArray * zoneNames = [NSMutableArray array];
        
        [self.ArrHotspot sortUsingComparator:^NSComparisonResult(id obj1, id obj2) {
            NSNumber * number1 = [NSNumber numberWithInt:[(Hotspots*)obj1 Hotspotdistance]];
            NSNumber * number2 = [NSNumber numberWithInt:[(Hotspots*)obj2 Hotspotdistance]];
            return [number1 compare:number2];
        }];
        
        for (int i=0; i<[self.ArrHotspot count]; i++) {
            Hotspots *obj= [self.ArrHotspot objectAtIndex:i];
            
            
            
            [self debugHotspot:obj index:i];
            

            if (obj.track.volume > 0.1) {
                 [zoneNames addObject: obj.Name];
            }
        }
        

        
        self.siteNameLabel.text = [zoneNames componentsJoinedByString:@","];
    }

    
//    for (AEAudioFilePlayer* channel in self.audioController.channels) {
//        for (Hotspots* obj in self.ArrHotspot) {
//            if (obj.track == channel) {
//                NSLog(@"%@ == %@", obj.Name, channel.url);
//                break;
//            }
//        }
//    }

}

- (void)debugHotspot:(Hotspots*)obj index:(int)i
{
    dispatch_async(dispatch_get_main_queue(), ^{
        UILabel *label = (UILabel*)[self.view viewWithTag:i+10];
#ifdef DEBUG
        label.hidden = YES; //NO;
#endif
        label.text=[NSString stringWithFormat:@"d: %d, %@ r: %@ v: %.2f", obj.Hotspotdistance, obj.Name, obj.Radius, obj.track.volume];
    });
}

-(void)setHotspotsVolume
{
    
    if (isFirstPing==1) {
        
        for (int i=0; i<[self.ArrHotspot count]; i++) {
            Hotspots *obj= [self.ArrHotspot objectAtIndex:i];
            if (obj.Hotspotdistance>=0 && obj.LastHotspotdistance>=0 && obj.LastHotspotdistance>obj.Hotspotdistance)
            {
                if (obj.LastHotspotdistance>=0 && obj.LastHotspotdistance<=[obj.Radius intValue])
                    obj.track.volume = obj.track.volume+0.01;
                obj.LastHotspotdistance--;
                
            }
            else if (obj.Hotspotdistance>=0 && obj.LastHotspotdistance>=0 && obj.LastHotspotdistance<obj.Hotspotdistance)
            {
                if (obj.LastHotspotdistance>=0 && obj.LastHotspotdistance<[obj.Radius intValue])
                    obj.track.volume = obj.track.volume-0.01;
                obj.LastHotspotdistance++;
            }
            
            [self debugHotspot:obj index:i];

            
            if (obj.track.volume > 0.1) {
                self.siteNameLabel.text = obj.Name;
            }
        }
    }
    
}

- (void)parser:(NSXMLParser *)parser didStartElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qualifiedName attributes:(NSDictionary *)attributeDict
{
    
    if([elementName isEqualToString:@"Composition"]) {
        //Initialize the array.
        BBComposition=[[Composition alloc] init];
        BBComposition.ArrZone= [[NSMutableArray alloc] init];
        BBComposition.ArrCompositionCorner= [[NSMutableArray alloc] init];
        parentTag=elementName;
    }
    else if([elementName isEqualToString:@"Corner"]) {
        
        BBCorners = [[Corners alloc] init];
    }
    else if([elementName isEqualToString:@"Zone"]) {
        
        BBZone = [[Zone alloc] init];
        BBZone.ArrZoneCorner= [[NSMutableArray alloc] init];
        BBZone.ArrHotspot= [[NSMutableArray alloc] init];
        parentTag=elementName;
    }
    else if([elementName isEqualToString:@"Hotspot"]) {
        
        CurrentHotspot = [[Hotspots alloc] init];
        parentTag=elementName;
    }

}

// This delegate method gets called as a new start. In this, we allocate all data which will hold the data.

- (void)parser:(NSXMLParser *)parser foundCharacters:(NSString *)string
{
    if(!curElem)
        curElem = [[NSMutableString alloc] initWithString:string];
    else
        [curElem appendString:string];
}

- (void)parser:(NSXMLParser *)parser didEndElement:(NSString *)elementName namespaceURI:(NSString *)namespaceURI qualifiedName:(NSString *)qName
{
    NSString *elementValue=[NSString stringWithString:curElem];
    elementValue=[elementValue stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    curElem = nil;
    
    if([elementName isEqualToString:@"Name"]) {
        
        if ([parentTag isEqualToString:@"Composition"])
        {
            [BBComposition setValue:elementValue forKey:elementName];
        }
        if ([parentTag isEqualToString:@"Zone"]) {
            [BBZone setValue:elementValue forKey:elementName];
        }
        if ([parentTag isEqualToString:@"Hotspot"]) {
            [CurrentHotspot setValue:elementValue forKey:elementName];
        }
        
    }
    
    if([elementName isEqualToString:@"Latitude"]) {
        
        if ([parentTag isEqualToString:@"Hotspot"]) {
            [CurrentHotspot setValue:elementValue forKey:elementName];
        }
        else
        {
            [BBCorners setValue:elementValue forKey:elementName];
        }
    }
    
    if([elementName isEqualToString:@"Longitude"]) {
        
        if ([parentTag isEqualToString:@"Hotspot"]) {
            [CurrentHotspot setValue:elementValue forKey:elementName];
        }
        else
        {
            [BBCorners setValue:elementValue forKey:elementName];
        }
        
    }
    
    if([elementName isEqualToString:@"Filename"]) {
        
        [CurrentHotspot setValue:elementValue forKey:elementName];
    }
    
    if([elementName isEqualToString:@"Looping"]) {
        
        [CurrentHotspot setValue:elementValue forKey:elementName];
        
    }
    
    if([elementName isEqualToString:@"Radius"]) {
        
        [CurrentHotspot setValue:elementValue forKey:elementName];
    }
    if([elementName isEqualToString:@"Hotspot"]) {
        
        [BBZone.ArrHotspot addObject:CurrentHotspot];
        CurrentHotspot=nil;
    }
    
    if([elementName isEqualToString:@"Corner"]) {
        
        if ([parentTag isEqualToString:@"Composition"]) {
            [BBComposition.ArrCompositionCorner addObject:BBCorners];
        }
        if ([parentTag isEqualToString:@"Zone"]) {
            [BBZone.ArrZoneCorner addObject:BBCorners];
        }
        
        BBCorners = nil;
        
    }
    
    if([elementName isEqualToString:@"Zone"]) {
        
        [BBComposition.ArrZone addObject:BBZone];
        BBZone=nil;
    }
    
    if([elementName isEqualToString:@"Composition"])
        return;
    
}

-(NSMutableArray*)getCurrentZone
{
    for (int i=0; i<[BBComposition.ArrZone count]; i++)
    {
        
        Zone *Zobj= [BBComposition.ArrZone  objectAtIndex:i];

            if ([self.ArrHotspot count]==0)
            {
                self.ArrHotspot=Zobj.ArrHotspot;
                [self setupAudioFile];
            }
            else
            {
                
                int flag=0;
                for (int i=0; i<[self.ArrHotspot count]; i++)
                {
                    Hotspots *tempObj= [self.ArrHotspot objectAtIndex:i];
                    if (tempObj.LastHotspotdistance<=[tempObj.Radius intValue])
                    {
                        flag=1;
                        break;
                    }
                    
                }
                if (flag==0)
                {
                    [self removeAudioFile];
                    self.ArrHotspot=[[NSMutableArray alloc] init];
                    self.ArrHotspot=Zobj.ArrHotspot;
                    [self setupAudioFile];
                }
            }
            break;
        
    }
    
    return self.ArrHotspot;
}

- (BOOL)isWithinZone:(CLLocation *)position point1:(CLLocationCoordinate2D)point1 point2:(CLLocationCoordinate2D)point2 point3:(CLLocationCoordinate2D)point3 point4:(CLLocationCoordinate2D)point4
{
    
    if (PointInTriangle(position.coordinate, point1, point2, point3) || PointInTriangle(position.coordinate, point2, point3, point4) || PointInTriangle(position.coordinate, point1, point3, point4) || PointInTriangle(position.coordinate, point1, point2, point4)) {
        return YES;
    }
    return NO;
}

bool PointInTriangle(CLLocationCoordinate2D pt, CLLocationCoordinate2D v1, CLLocationCoordinate2D v2, CLLocationCoordinate2D v3)
{
    bool b1, b2, b3;
    
    b1 = sign(pt, v1, v2) < 0.0f;
    b2 = sign(pt, v2, v3) < 0.0f;
    b3 = sign(pt, v3, v1) < 0.0f;
    
    return ((b1 == b2) && (b2 == b3));
}

float sign(CLLocationCoordinate2D p1, CLLocationCoordinate2D p2, CLLocationCoordinate2D p3)
{
    return (p1.longitude - p3.longitude) * (p2.latitude - p3.latitude) - (p2.longitude - p3.longitude) * (p1.latitude - p3.latitude);
}


- (IBAction)viewWebsite:(id)sender
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.hrholladay.com/"]];
}

- (IBAction)openEmailComposer:(id)sender
{
    if ([MFMailComposeViewController canSendMail]) {
        // Show the composer
        if (self.mailComposeViewController == nil) {
            self.mailComposeViewController = [[MFMailComposeViewController alloc] init];
        } // bug in iOS8 simulator, email conposer auto close?!
        self.mailComposeViewController.mailComposeDelegate = self;
        [self.mailComposeViewController setToRecipients:@[@"rholladay@gmail.com"]];
        [self.mailComposeViewController setSubject:@""];
        [self.mailComposeViewController setMessageBody:@"" isHTML:NO];
        if (self.mailComposeViewController) [self presentViewController:self.mailComposeViewController animated:YES completion:NULL];
    } else {
        // Handle the error
    }
    

}

# pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }else{
        NSLog(@"error: %@", error);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    self.mailComposeViewController = nil;
}


#pragma mark - Map
const  NSInteger TAG = 123;
- (IBAction)loadMapView:(id)sender
{

    self.mapView = (MKMapView *)[self.view viewWithTag:TAG];
    if (self.mapView) {
        
    }else{
        self.mapView = [[MKMapView alloc] initWithFrame:self.view.frame];
        self.mapView.tag = TAG;
        [self.view addSubview:self.mapView];
        
        float x = self.view.frame.size.width - 50;
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(x, 10, 36, 36)];
        [btn setBackgroundImage:[UIImage imageNamed:@"close"] forState:UIControlStateNormal];
        [btn addTarget:self action:@selector(closeMapView) forControlEvents:UIControlEventTouchUpInside];
        [self.mapView addSubview:btn];
        
        UIButton *centerBtn = [[UIButton alloc] initWithFrame:CGRectMake(x, 50, 34, 34)];
        [centerBtn setBackgroundImage:[UIImage imageNamed:@"compass"] forState:UIControlStateNormal];
        [centerBtn addTarget:self action:@selector(centerToUserLocation) forControlEvents:UIControlEventTouchUpInside];
        [self.mapView addSubview:centerBtn];

        
        self.mapView.delegate = self;
        self.mapView.showsUserLocation = YES;
        self.mapView.mapType = MKMapTypeStandard;
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
        
        
        //    mapView?.addOverlay(MKCircle(centerCoordinate: geotification.coordinate, radius: geotification.radius))
        
        NSString * result = @"";
        for (int i=0; i<[self.ArrHotspot count]; i++) {
  
            Hotspots *obj= [self.ArrHotspot objectAtIndex:i];
          NSLog(@"adding hotspot %@, %@, %@", obj.Name, obj.Latitude, obj.Longitude);
            obj.Hotspot=[[CLLocation alloc] initWithLatitude:[obj.Latitude doubleValue] longitude:[obj.Longitude doubleValue]];
           
            [self.mapView addOverlay:[MKCircle circleWithCenterCoordinate:obj.Hotspot.coordinate radius:[obj.Radius floatValue]]];
            
            //generate gpx file
            
            result = [result stringByAppendingFormat:@"<wpt lat=\"%@\" lon=\"%@\"><name>%@</name></wpt>\n",
                      obj.Latitude,obj.Longitude,obj.Name];
            
        }
#ifdef DEBUG
        [result writeToFile:[[self applicationDocumentsDirectory] stringByAppendingString:@"/waypoint.gpx"]
                       atomically:YES];
#endif
        
    }
}
- (NSString *) applicationDocumentsDirectory
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *basePath = paths.firstObject;
    return basePath;
}
- (MKOverlayView *)mapView:(MKMapView *)map viewForOverlay:(id <MKOverlay>)overlay
{
    MKCircleView *circleView = [[MKCircleView alloc] initWithOverlay:overlay];
//    circleView.strokeColor = [UIColor redColor];
    circleView.fillColor = [[UIColor redColor] colorWithAlphaComponent:0.4];
    return [circleView autorelease];
}

- (void)closeMapView
{
    
    if (self.mapView) {
        self.mapView.delegate = nil;
        [self.mapView removeFromSuperview];
        self.mapView = nil;
    }
}

- (void)centerToUserLocation
{
    if (self.mapView.userLocation) {
        
    
//        MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(self.mapView.userLocation.coordinate, 500, 500);
//        [self.mapView setRegion:region animated:YES];
//        [self.mapView setCenterCoordinate:self.mapView.userLocation.coordinate animated:YES];
        [self.mapView setUserTrackingMode:MKUserTrackingModeFollow animated:YES];
    }
    
}

@end
