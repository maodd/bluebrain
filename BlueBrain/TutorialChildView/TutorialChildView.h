//
//  TutorialChildView.h
//  BlueBrain
//
//  Copyright (c) 2014 Capitalnumbers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorialChildView.h"
#import "MainScreen.h"

@interface TutorialChildView : UIViewController

@property NSInteger indexOfPage;
 
@property (retain, nonatomic) IBOutlet UITextView *tutorial1TxtView;


- (IBAction)okayBtnClicked:(id)sender;


@end
