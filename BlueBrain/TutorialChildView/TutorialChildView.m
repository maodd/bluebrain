//
//  TutorialChildView.m
//  BlueBrain
//
//  Copyright (c) 2014 Capitalnumbers. All rights reserved.
//

#import "TutorialChildView.h"

@interface TutorialChildView ()

@end

@implementation TutorialChildView
@synthesize indexOfPage;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
//    NSDictionary *  bkColorInfo = [BlueBrainAppDelegate sharedInstance].appCfg[@"IntroViewBackgroundColor"];
//    
//    _secondMainView.backgroundColor = [UIColor colorWithRed:[bkColorInfo[@"red"] floatValue]/255.0f
//                                                      green:[bkColorInfo[@"green"] floatValue]/255.0
//                                                       blue:[bkColorInfo[@"blue"] floatValue]/255.0
//                                                      alpha:1.0];
    
    //    NSDictionary *  tintColorInfo = [BlueBrainAppDelegate sharedInstance].appCfg[@"PageIndicatorTintColor"];
    //
    //    [_okatBtnOutlet setBackgroundColor:[UIColor colorWithRed:[tintColorInfo[@"red"] floatValue]/255.0f
    //                                                       green:[tintColorInfo[@"green"] floatValue]/255.0f
    //                                                        blue:[tintColorInfo[@"blue"] floatValue]/255.0f
    //                                                       alpha:1]];
    //
    //    NSDictionary *  fontColorInfo = [BlueBrainAppDelegate sharedInstance].appCfg[@"IntroViewButtonTextColor"];
    //
    //
    //    [_okatBtnOutlet setTitleColor:[UIColor colorWithRed:[fontColorInfo[@"red"] floatValue]/255.0f
    //                                                 green:[fontColorInfo[@"green"] floatValue]/255.0f
    //                                                  blue:[fontColorInfo[@"blue"] floatValue]/255.0f
    //                                                 alpha:1] forState:UIControlStateNormal];
    //
    //    [_okatBtnOutlet setTitle:[BlueBrainAppDelegate sharedInstance].appCfg[@"IntroViewButtonText"] forState:UIControlStateNormal];
    //
    //    _okatBtnOutlet.layer.cornerRadius = 5;//half of the width
    
 
    
//    _tutorial1TxtView.font = [UIFont fontWithName:[BlueBrainAppDelegate sharedInstance].appCfg[@"IntroViewTextFontName"] size:[[BlueBrainAppDelegate sharedInstance].appCfg[@"IntroViewTextFontSize"] floatValue] ];
    
    NSArray * introText = [BlueBrainAppDelegate sharedInstance].appCfg[@"LaunchScreenIntroText"];
    
    if (indexOfPage == 0) {
        _tutorial1TxtView.text = introText[0];
       
        
    }else if (indexOfPage==1){
        _tutorial1TxtView.text = introText[1];
        
        
    }else if (indexOfPage==2){
        _tutorial1TxtView.text = introText[2];
        
 
        
    }
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)okayBtnClicked:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tutorial_played"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    MainScreen *objMainScreen;
 
    {
        objMainScreen = [[MainScreen alloc] initWithNibName:@"MainScreen_iPhone" bundle:nil];
    }
    
    [self.navigationController pushViewController:objMainScreen animated:YES];
//    MainScreen * objMainScreen = [BlueBrainAppDelegate sharedInstance].objViewController.mainScreen;
//    UINavigationController * nvc = (UINavigationController*)[BlueBrainAppDelegate sharedInstance].window.rootViewController;
//    [nvc setViewControllers:@[objMainScreen] animated:false];
    
//    [self dismissViewControllerAnimated:true completion:^{

         
//    }];
    NSLog(@"Called");
}
@end
