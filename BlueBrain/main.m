//
//  main.m
//  BlueBrain
//
//  Created by Bijay Ghosh on 9/24/13.
//  Copyright (c) 2013 Capital Numbers. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "BlueBrainAppDelegate.h"

int main(int argc, char *argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([BlueBrainAppDelegate class]));
    }
}
