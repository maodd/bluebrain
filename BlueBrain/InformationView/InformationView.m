//
//  InformationView.m
//  BlueBrain
//
//  Copyright (c) 2014 Capitalnumbers. All rights reserved.
//

#import "InformationView.h"
#import <QuartzCore/QuartzCore.h>
CGRect screenRect;
CGFloat screenWidth;
CGFloat screenHeight;

@interface InformationView ()
@property (nonatomic, strong) MFMailComposeViewController* mailComposeViewController;


@end


@implementation InformationView

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    [self.textView setLinkTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:255/255.0 green:200/255.0 blue:10/255.0 alpha:1.0]}];
                                        
//    NSDictionary *  bkColorInfo = [BlueBrainAppDelegate sharedInstance].appCfg[@"IntroViewBackgroundColor"];
    
//    _secondView.backgroundColor = [UIColor colorWithRed:[bkColorInfo[@"red"] floatValue]/255.0f
//                                                      green:[bkColorInfo[@"green"] floatValue]/255.0
//                                                       blue:[bkColorInfo[@"blue"] floatValue]/255.0
//                                                      alpha:1.0];
 
    if([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0)
    {
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone){
            _secondView.frame = CGRectMake(0, 0, 320, 480);
        }
        else{
            _secondView.frame = CGRectMake(0, 0, 768, 1024);
        }
    }
    
    [_secondView addSubview:_scrolViewInfo];
    
    screenRect = [[UIScreen mainScreen] bounds];
    screenWidth = screenRect.size.width;
    screenHeight = screenRect.size.height;
    if (screenHeight == 480.0) {
         _scrolViewInfo.contentSize = CGSizeMake(320, 2200); //1020
    }
    else
    {
        _scrolViewInfo.contentSize = CGSizeMake(320, 2200); //1020
    }

    // Do any additional setup after loading the view from its nib.
    self.contactButton.layer.borderWidth=1.0f;
    self.contactButton.layer.borderColor=[[UIColor colorWithRed:246/255.0
                                                          green:190/255.05
                                                           blue:84/255.0 alpha:1] CGColor];

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)mailBtnClicked:(id)sender {
    [self openEmailComposer:sender];
}

- (IBAction)cancelBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)openEmailComposer:(id)sender
{
    if ([MFMailComposeViewController canSendMail]) {
        // Show the composer
        if (self.mailComposeViewController == nil) {
            self.mailComposeViewController = [[MFMailComposeViewController alloc] init];
        } // bug in iOS8 simulator, email conposer auto close?!
        self.mailComposeViewController.mailComposeDelegate = self;
        [self.mailComposeViewController setToRecipients:@[@"rholladay@gmail.com"]];
        [self.mailComposeViewController setSubject:@""];
        [self.mailComposeViewController setMessageBody:@"" isHTML:NO];
        if (self.mailComposeViewController) [self presentViewController:self.mailComposeViewController animated:YES completion:NULL];
    } else {
        // Handle the error
    }
    
    
}

# pragma mark - MFMailComposeViewControllerDelegate

- (void)mailComposeController:(MFMailComposeViewController*)controller
          didFinishWithResult:(MFMailComposeResult)result
                        error:(NSError*)error;
{
    if (result == MFMailComposeResultSent) {
        NSLog(@"It's away!");
    }else{
        NSLog(@"error: %@", error);
    }
    [self dismissViewControllerAnimated:YES completion:NULL];
    self.mailComposeViewController = nil;
}
@end
