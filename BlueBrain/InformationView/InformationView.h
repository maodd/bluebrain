//
//  InformationView.h
//  BlueBrain
//
//  Copyright (c) 2014 Capitalnumbers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface InformationView : UIViewController<MFMailComposeViewControllerDelegate>
@property (retain, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (retain, nonatomic) IBOutlet UIView *secondView;
@property (retain, nonatomic) IBOutlet UITextView *textView;
@property (retain, nonatomic) IBOutlet UIScrollView *scrolViewInfo;
@property (retain, nonatomic) IBOutlet UIButton *contactButton;
- (IBAction)mailBtnClicked:(id)sender;
- (IBAction)cancelBtnClicked:(id)sender;

@end
