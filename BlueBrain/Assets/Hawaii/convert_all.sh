#!/bin/bash
for i in *.ogg; do
  j=$(echo -n $i | sed -e 's/.ogg/.m4a/g')
  echo "converting $i  to  $j"
  ffmpeg -y -i "$i" -strict experimental -acodec aac  "$j"
done
