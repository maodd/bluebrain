//
//  BlueBrainAppDelegate.m
//  BlueBrain
//
//  Created by Bijay Ghosh on 9/24/13.
//  Copyright (c) 2013 Capital Numbers. All rights reserved.
//

#import "BlueBrainAppDelegate.h"
#import <Fabric/Fabric.h>



@implementation BlueBrainAppDelegate
@synthesize objViewController,nav;


+ (BlueBrainAppDelegate*)sharedInstance
{
    return (BlueBrainAppDelegate*)[UIApplication sharedApplication].delegate;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    sleep(2); // longer spalsh

    
    
    
    // Find out the path of recipes.plist
    NSString *path = [[NSBundle mainBundle] pathForResource:@"AppCfg" ofType:@"plist"];
    
    // Load the file content and read the data into arrays
    self.appCfg = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    
    
    if ([[[UIDevice currentDevice] systemVersion] floatValue] >= 6.0) {
        UIPageControl *pageControl = [UIPageControl appearance];
        pageControl.pageIndicatorTintColor = [UIColor whiteColor];
        NSDictionary * tintColorInfo = [self appCfg][@"PageIndicatorTintColor"];
        pageControl.currentPageIndicatorTintColor = [UIColor colorWithRed:[tintColorInfo[@"red"] floatValue]/255.0f
                                                                    green:[tintColorInfo[@"green"] floatValue]/255.0f
                                                                     blue:[tintColorInfo[@"blue"] floatValue]/255.0f alpha:1.0];
        
        pageControl.pageIndicatorTintColor = [UIColor grayColor];
        pageControl.backgroundColor = [UIColor clearColor];
    }
    
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    
    // Override point for customization after application launch.
    
    objViewController = [ViewController new];
    nav = [[UINavigationController alloc] initWithRootViewController:objViewController];
    self.window.rootViewController = nav;
    [nav setNavigationBarHidden:YES];
    [self.window makeKeyAndVisible];
    

    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

# pragma mark - Utils

- (NSString*)appName
{
    return [[NSBundle mainBundle] objectForInfoDictionaryKey:@"CFBundleDisplayName"];
}

@end
