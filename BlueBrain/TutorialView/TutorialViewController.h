//
//  TutorialViewController.h
//  BlueBrain
//
//  Copyright (c) 2014 Capitalnumbers. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TutorialChildView.h"

@interface TutorialViewController : UIViewController<UIPageViewControllerDelegate,UIPageViewControllerDataSource>
@property (retain, nonatomic) IBOutlet UIImageView *backgrondImageView;
@property (retain, nonatomic) IBOutlet UIView *secondMainView;
@property (strong, nonatomic) UIPageViewController *pageController;

@property (retain, nonatomic) IBOutlet UIPageControl *pageControl;
@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;
@end
