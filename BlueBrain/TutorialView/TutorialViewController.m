//
//  TutorialViewController.m
//  BlueBrain
//
//  Copyright (c) 2014 Capitalnumbers. All rights reserved.
//

#import "TutorialViewController.h"
#import "TutorialChildView.h"

@interface TutorialViewController ()


@end

@implementation TutorialViewController


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW,
                                 2 * NSEC_PER_SEC),
                   dispatch_get_main_queue(),
                   ^{
                       // Do whatever you want here.
                       self.secondMainView.hidden = true;
                   });
    
    
}

- (IBAction)okayBtnClicked:(id)sender {
    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"tutorial_played"];
    [[NSUserDefaults standardUserDefaults] synchronize];
    
    MainScreen *objMainScreen;
    
    {
        objMainScreen = [[MainScreen alloc] initWithNibName:@"MainScreen_iPhone" bundle:nil];
    }
    
    [self.navigationController pushViewController:objMainScreen animated:YES];
    //    MainScreen * objMainScreen = [BlueBrainAppDelegate sharedInstance].objViewController.mainScreen;
    //    UINavigationController * nvc = (UINavigationController*)[BlueBrainAppDelegate sharedInstance].window.rootViewController;
    //    [nvc setViewControllers:@[objMainScreen] animated:false];
    
    //    [self dismissViewControllerAnimated:true completion:^{
    
    
    //    }];
    NSLog(@"Called");
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    CGFloat pageWidth = self.scrollView.frame.size.width;
    float fractionalPage = self.scrollView.contentOffset.x / pageWidth;
    NSInteger page = lround(fractionalPage);
    self.pageControl.currentPage = page;
}


@end
